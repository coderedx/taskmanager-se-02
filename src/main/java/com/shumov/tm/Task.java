package com.shumov.tm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class Task {

    private static List<Task> list = new ArrayList<>();

    private String id;
    private String name;
    private String description;
//    private Date startDate;
//    private Date endDate;
//    private Project project;

    public Task(String name){
        UUID uuid = UUID.randomUUID();
        this.id = uuid.toString();
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }

//    public Date getStartDate() {
//        return startDate;
//    }
//
//    public Date getEndDate() {
//        return endDate;
//    }
//
//    public Project getProject() {
//        return project;
//    }

    public static List<Task> getList() {
        return list;
    }

//    public void setProject(Project project) {
//        this.project = project;
//    }
}
